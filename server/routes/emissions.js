"use strict";

module.exports = function(app) {
    var controller = require('../controllers/emissionsController');
    app.route('/api/global/:yearId/emissions')
      .get(controller.findByYear)
    
    app.route('/api/global/:yearId/tenfirst')
      .get(controller.findTenFirst)
}
