# Import CSV file in Mongo Database

    mongoimport --host YOUR_HOSTNAME --port YOUR_PORT --authenticationDatabase admin --username USERNAME --password PASSWORT --collection COLLECTION_NAME --db YOUR_DATABASE  --type csv --headerline  --file FILEPATH
