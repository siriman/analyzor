"use strict";

var mongoose    = require('../models/config');
    Global      = mongoose.model('Global');

var eu = ["Austria", "Bulgaria", "Belgium", "Croatia", "Cyprus", "Czech Republic", "Denmark", "Estonia", "Finland",
    "France", "Germany", "Greece", "Hungary", "Ireland", "Italy", "Latvia", "Lithuania", "Luxembourg", "Malta", "Netherlands", 
    "Poland", "Portugal", "Romania", "Slovakia", "Slovenia", "Spain", "Sweden", "United Kingdom"];

exports.findByYear = function(req, res) {
    Global.find({ 'Year': req.params.yearId, 'Country': { $nin: eu } }, function(err, emissions) {
        if (err) throw err;
        //treatData(emissions);
        res.status(200).json(treatData(emissions));
        res.end();
    }).sort( { 'Total GHG Emissions Excluding Land-Use Change and Forestry (MtCO2e)': -1 } );
};

exports.findTenFirst = function(req, res){
    Global.find({ 'Year': req.params.yearId }, function(err, emissions) {
        if (err) throw err;
        res.status(200).json(emissions);
        res.end();
    });
};

function treatData(json){ 
    var tmp = [];
    var world = json.splice(0, 1);
    json.forEach(function(elt) {
        tmp.push(createItem(elt));           
    });
    
    var top = {
        "name": 'Top 10',
        "children": tmp.splice(0, 10)
    };
    var other = {
        "name": 'Others',
        "children": tmp
    };
    var res = {
        "name": 'All emissions',
        "total": world[0]["Total GHG Emissions Excluding Land-Use Change and Forestry (MtCO2e)"],        
        "children": [top, other]
    };

    return res;
}

function createItem(obj){
    var item = {
        name: String,
        children: []
    };

    var energie     = { name: 'Energy', size: Number };
    var industry    = { name: 'Industry', size: Number };
    var agricul     = { name: 'Agriculture', size: Number };
    var waste       = { name: 'Waste', size: Number };
    
    item.name       = obj.Country;
    energie.size    = obj[ 'Energy (MtCO2e)' ];
    industry.size   = obj[ 'Industrial Processes (MtCO2e)' ] 
    agricul.size    = obj[ 'Agriculture (MtCO2e)' ] 
    waste.size      = obj[ 'Waste (MtCO2e)' ] 
    
    item.children.push(energie);
    item.children.push(industry);
    item.children.push(agricul);
    item.children.push(waste);

    return item;
}
