"use strict";

var mongoose = require('./config');
var Schema = mongoose.Schema;

var globalSchema = new Schema({ 
    'Country': String, 
    'Year': Number, 
    'Total GHG Emissions Excluding Land-Use Change and Forestry (MtCO2e)': Number,
    'Total GHG Emissions Including Land-Use Change and Forestry (MtCO2e)': Number,
    'Total CO2 (excluding Land-Use Change and Forestry) (MtCO2)': Number,
    'Total CH4 (MtCO2e)': Number,
    'Total N2O (MtCO2e)': Number,
    'Total F-Gas (MtCO2e)': Number,
    'Total CO2 (including Land-Use Change and Forestry) (MtCO2)': Number,
    'Total CH4 (including Land-Use Change and Forestry) (MtCO2e)': Number,
    'Total N2O (including Land-Use Change and Forestry) (MtCO2e)': Number,
    'Energy (MtCO2e)': Number,
    'Industrial Processes (MtCO2e)': Number,
    'Agriculture (MtCO2e)': Number,
    'Waste (MtCO2e)': Number,
    'Land-Use Change and Forestry (MtCO2)': Number,
    'Bunker Fuels (MtCO2)': Number,
    'Electricity/Heat (MtCO2)': Number,
    'Manufacturing/Construction (MtCO2)': Number,
    'Transportation (MtCO2)': Number,
    'Other Fuel Combustion (MtCO2e)': Number,
    'Fugitive Emissions (MtCO2e)': Number
 }, { collection: 'global' });
module.exports = mongoose.model('Global', globalSchema);
