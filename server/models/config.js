var mongoose        = require('mongoose');

mongoose.connect(process.env.DB_URI, {
    useMongoClient: true
}, function(){
    console.log("Database connection is OK")
})

module.exports = mongoose;
