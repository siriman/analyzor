require('dotenv').config();

var express         = require('express'),
    request         = require('request')
    bodyParser      = require('body-parser'),
    emissions       = require('./routes/emissions'),
    cors            = require('cors'),
    app             = express(),

    Global          = require('./models/emissionsModel'); //created model loading here

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var originsWhitelist = [
    'http://localhost:4200',      //this is my front-end url for development

    'http://www.myproductionurl.com'
];
var corsOptions = {
    origin: function(origin, callback){
        var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
        callback(null, isWhitelisted);
    },
    credentials:true
}

app.use(cors(corsOptions));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//app.use('/api/global', emissions);
emissions(app);

app.listen("8081");
console.log('RESTful API server started on : 8081');
