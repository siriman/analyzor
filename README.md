# Analyzor
This project consits to design a tool that give a intelligent visualization of the climate change data.

**Getting started**

 - Clone project source in gitlab by :
`git clone git@gitlab.com:SirRoot/analyzor.git` or `git clone https://gitlab.com/SirRoot/analyzor.git`
 - Create **.env** file in **server** folder. This file contains the database access information. For example it look like : `DB_URI = 'mongodb://USERNAME:PASSWORD@HOSTNAME:PORT/DB_NAME?authSource=admin'
`
- Import all **csv** file content in **data** folder


**Prerequisites**

Node js v.4.2.*, MongoDb

**Installation**

 - In **server** folder run `npm install`
 - When you use a local mongo, you need to start it with `mongo start`
 - Open **index.html** in **client** in browser

 Hope the app is ready to test !!!

**Authors**

 - Siriman TRAORE 21308390 [21308390@etu.unicaen.fr](21308390@etu.unicaen.fr)
 - Lofti IDIR 21613320 [21613320@etu.unicaen.fr](21613320@etu.unicaen.fr)